README v1.0 / 02 May 2018

# theKhabar

## Introduction

A news streaming app where users can login and add comments and likes and admin login can add,delete articles and moderate comments.

## Usage

Simply run python3 app.py and open local host

## Contributing

Give instructions on how to participate in your project patches.

## Help

Explain which communication channels are available to request help. Communication channels with a proven track record are mailing lists, IRC channels, and forums. Also be sure to tell your more experienced users how and where to submit bugs or feature requests, possibly turning them into project participants.

## Installation

### Requirements

List anything your project requires in order to work as expected.

### Installation

Describe how to install your program. Be precise and give examples. Don't assume your users know how to clone from my github repo. Keep in mind that some of your users may be completely unskilled in system administration or software development.

### Configuration

After having installed the software, the user may need to configure it. List configuration options and explain how and where to set them.

## Credits

Sometimes also called Authors, this is the list of project contributors.

## Contact

People may want to reach out to you for various reasons, ranging from DCMA take down notices to questions about how to donate to your project. Provide contact information, such as an email address, and keep in mind that some countries may require certain information by law, such as a full postal address, website URL, and company name. 